---
layout: handbook-page-toc
title: "Product Certification for GitLab Field Team Members"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The objective of this program is to ensure field team members have the requisite knowledge to serve as trusted advisors to customers, prospects, and partners by properly positioning the right GitLab solution(s) based on customers’ or prospects’ expressed needs and/or challenges. 

Note: This GitLab product training and certification program is different from resources like the ones below that focus on how to _use GitLab_ (though we encourage field team members to take these as well)! 
- [GitLab Certifications](/handbook/people-group/learning-and-development/certifications/)
- [GitLab Technical Certifications](/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/)

## Prerequisites

During [Sales & Customer Success onboarding](/handbook/sales/onboarding/), every GitLab field team member is trained on [customer value drivers](/handbook/sales/command-of-the-message/#customer-value-drivers) and GitLab’s value-based messaging framework (see the [Command of the Message](/handbook/sales/command-of-the-message/) page). In addition, GitLab field team members and partners are introduced to GitLab’s [customer use cases](/handbook/use-cases/).

## Architecture and Approach

### What the Field Needs to Know

The above customer value-based foundation extends to product training and certification. More specifically, learning objectives are determined by the [Use Case Activation Teams](/handbook/marketing/strategic-marketing/usecase-gtm/#use-case-activation-team) for all [Tier 1](/handbook/marketing/strategic-marketing/usecase-gtm/#tier-1) and [Tier 2](/handbook/marketing/strategic-marketing/usecase-gtm/#tier-2) customer use cases based on what and how key GitLab features and capabilities deliver against prioritized [market requirements](/handbook/marketing/strategic-marketing/usecase-gtm/#market-requirements) of each customer use case. Learning objectives are also defined for what the field needs to know about GitLab’s [product tiers](/handbook/marketing/strategic-marketing/tiers/). Two sets of prioritized product learning objectives are maintained--one for Sales roles and one for Customer Success roles.

### Training 

Lightweight role-based product training learning paths (one for Sales roles and one for Customer Success roles) are being developed to support the learning objectives outlined above with the goal of launching by or before early FY21. Until that time, team members and partners are encouraged to leverage and consume existing Handbook resources and participate in existing informal continuing education programs. New field team members will be encouraged to complete this training during their first few months in the role after successfully completing the [Sales Quick Start field onboarding](/handbook/sales/onboarding/) training program.

### Certification

New team members will earn their GitLab product certification after completing the above learning path and scoring 80% or higher on the appropriate role-based product knowledge assessment.

Because GitLab launches a new release on the 22nd of every month, role-based product knowledge learning objectives, assessments, and learning paths will be refreshed ona regular cadence by the Use Case Activation Teams, Product Marketing, and Field Enablement. Likewise, field team members must be recertified every six months (in early Q1 and again in early Q3). To be recertified, field team members must score 80% or higher on the appropriate role-based product knowledge assessment. If a team member scores less than 80%, they must complete the appropriate role-based product training learning path and will then have a chance to retake the knowledge assessment until they score 80% or higher. GitLab partner certifications will leverage the same training and assessment content (details TBD).

Current product knowledge assessments:
- GitLab Product Knowledge Assessment for Sales roles (coming soon)
- GitLab Product Knowledge Assessment for Customer Success roles (coming soon)

## How to Sell GitLab

Additional deeper dive customer use case training focused on **how to sell GitLab** will be made available to field team members and partners and include the below elements for each:
- Market Overview
- Target Buyer and User Personas
- Industry Insights
- Market Requirements
- How GitLab Does It
- How GitLab Does it Better
- Getting the Meeting
- Proof Points
- Competitive Comparisons
- Application Exercise

### Customer Use Case Training 

Currently available courses include the following:

#### Continuous Integration (CI)

- Access the [Continuous Integration Customer Use Case learning path](https://classroom.google.com/c/ODA3MDM2NDkzODFa?cjc=4oy74tz) in Google Classroom (internal only)
- Just want to take the knowledge check quiz? [Click here](https://forms.gle/SYcjEptqKyfohUHx7). 

#### DevSecOps

- Access the [DevSecOps Customer Use Case learning path](https://classroom.google.com/c/MTI4MzkzMDA0NTg5?cjc=g3q2snm) in Google Classroom (internal only)
- Just want to take the knowledge check quiz? [Click here](TBD) (coming soon).
