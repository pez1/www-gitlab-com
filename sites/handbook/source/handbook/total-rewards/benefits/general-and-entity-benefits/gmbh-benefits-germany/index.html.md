---
layout: handbook-page-toc
title: "GitLab GmbH (Germany) benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Employees Based in Germany

Currently, GitLab does not provide additional benefits over and above the mandatory state requirements. [General GitLab benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/#general-benefits) still apply. As part of the [guiding principles](/handbook/total-rewards/benefits/#guiding-principles) this will be reviewed.

## German Social Security System

GitLab and team members contributions to pension, health insurance, care in old age insurance and unemployment insurance are mandatory, as required by the state system. The payments are calculated each month by payroll and are shown on the employee pay-slips.

Further information can also be found on the [Germany Trade & Invest Website](https://www.gtai.de/gtai-en/invest/investment-guide/employees-and-social-security/the-german-social-security-system-65600).

## Life Insurance

GitLab does not plan to offer Life Insurance Benefits because team members can access employer insurance and government pension schemes to help with payments in the event of a death of a family member.

## GitLab GmbH Germany Leave Policy

To initiate your Parental Leave, submit the dates in PTO by Roots under the Parental Leave category. This will prompt Total Rewards to process your leave. You can find out more information about our Parental Leave policy [here](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

**Maternity Protection**
The Maternity Protection (Mutterschutzgesetz) period is 6 weeks before the estimated date of birth and 8 weeks following the birth (or 12 weeks in case of multiple or premature birth). The team member will receive full payment during this period. GitLab may receive reimbursement of these payments from the team member's health insurance carrier. Please notify Total Rewards your health insurance carrier name and your insurance number.

**Parental Leave**
A team member is entitled to parental leave until the child turns three. Parental leave can be taken by the mother and father. 

For expecting mothers, they may take the Maternity Protection period followed by Parental Leave. GitLab will provide full Parental Lave pay (provided the team member meets the [eligibility criteria](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave)) for up to 16 weeks of the total leave (including the Maternity Protection period). The team member may apply for state benefits for the remainder of their Parental Leave.

For expecting fathers, team members will need to apply for a state benefit (Basiselterngeld, ElterngeldPlus or Partnerschaftsbonus). If a team member is [eligible for Parental Leave](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement Parental Leave pay for up to 16 weeks of Parental Leave. Please notify Total Rewards which state benefit you are applying for and how much you will be receiving so that payroll can process the appropriate supplementary amount. Team members can find further information on parental leave benefits via the [Federal Ministry for Family Affairs, Senior Citizens, Women and Youth](https://www.elterngeld-digital.de/ams/Elterngeld).


## Sick Time During COVID-19

During the COVID-19 Pandemic, per German labor law, team members are required to present a doctor's certificate if they need to take more than 3+ consecutive sick days. We encourage all team members to meet with a _virtual doctor_ for the certificate to avoid the need to leave home.
