---
layout: handbook-page-toc
title: "Customer Reference Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Customer reference program at GitLab

The goal of the Customer Reference Program is to provide opportunities for customers to share their story on how GitLab has helped them overcome the challenges, blockers and pain points within their organizations. The Reference Program Managers work as a conduit to help connect customers with opportunities by balancing customer requests with company demands. By providing a single point for outreach to customers, the CRP team prevents customer burnout, sales team burnout, and ensures a diversity of customer stories are shared publicly.

## Who we are
Team of Experienced Customer Reference Professionals who are focused on building relationships within GitLab and with customers.  
The team gathers and builds information and relationships with the purpose of distilling these impactful insights into action.

# Strategic driver of the Customer Reference Program
To manage our customer reference relationships like the precious resources they are to the maximum, quantified & qualified benefit for both customers and GitLab.

**Quick links grid**

| Quick Reference Links |      |
| ----------- | ----------- |
| [Adding new reference customers](/handbook/marketing/strategic-marketing/customer-reference-program/index.html#process-for-adding-new-reference-customers) | [Requesting a reference for a sales call](/handbook/marketing/strategic-marketing/customer-reference-program/index.html#requesting-a-reference-customer-to-support-a-sales-call)|
| [Customer Case Studies](/handbook/marketing/product-marketing/customer-reference-program/) | [Search published case studies by use case, value driver etc](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=%E2%9C%93&state=opened)|
| [Customer Reference Collection](/handbook/marketing/strategic-marketing/customer-reference-program/#customer-references-collection) |[Customer Case Study Slides (individual slides per case study)](https://docs.google.com/presentation/d/1YuP5_7LOnTMDcvl9UKPqim7lxaush4SUXDtTQ0uA4Sg/edit?usp=sharing) |
| [Requesting a customer to speak at an event](/handbook/marketing/strategic-marketing/customer-reference-program/#requesting-a-reference-customer-to-speak-andor-otherwise-support-an-event) | [Peer Reviews](/handbook/marketing/strategic-marketing/customer-reference-program/peer-reviews/) | [Customer Advisory Board](/handbook/marketing/strategic-marketing/customer-reference-program/CAB/) |
| [Offical Letter re Reference Process to share with customers](https://docs.google.com/document/d/19XGAnXrNQ5ngpnczlxE1p_7METY9S4_UFjh1vtiEZEg/edit) | [Customer Insight Page (Case Study Process)](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/)


## Goals of the GitLab Customer Reference Program
- **Credibility** - Reinforce our credibility as an end-to-end DevOps solution partner.
- **Shorten the sales cycle** - Having our advocates involved with sharing their story with potential sales, analysts and the marketplace can help potential sales close sooner
- **Increase Revenue** - A successful Customer Reference Program helps increase revenue by attracting new clients and increasing retention rates.
- **Customer Advocacy** - Encourage our customers to share their success stories with others who are learning about or evaluating GitLab.
- **Align with our customer value drivers** - Our program reflects our customer values drivers of increasing operational efficiencies, delivering better products faster and reducing security and compliance risk.
- **Align with our customer use cases** - Our program supports our [customer use cases](/handbook/use-cases/)

## Metrics of the GitLab Customer Reference Program
View information around customer reference pool, keep track of the latest case studies and find out how the Customer Reference team is helping share customer success stories.
- [Metrics Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/987)

### Customer references collection
Our written customer case studies are found on the [/customers page](https://about.gitlab.com/customers/)
Our video customer case studies are found on the YouTube site on [this playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ__Rq59rec-EMgKsLuNjJE)

### Customer Reference Types
Some examples of the types of assets we'd use as customer references once we have approval from the customer:
* Logo (The ability to name a company as a customer and use their logo in our marketing materials)
* Live sales reference (both calls and possible in-person)
* Website content
  * Written case studies (See [Customer Insight Page](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/) for case study process)
  * Blog posts
  * Videos
  * Podcasts
  * Usage quotes
* Event speakers (At industry, third-party and company events)
* References for analysts and press
* Quotes (can be attributable with approval or anonymous)

### Process for adding new reference customers
   1. Sales team member nominates champion by pushing the **Nominate a Reference** button on the individual contact (champion's name) in SFDC. 
   2. Sales team member works with the Technical Account Manager (TAM) to fill out the GitLab version customer is using, stages the customer is using and any information about the tech stack.
   3. CRM team is notified of nomination in SFDC, CRM creates a GitLab issue to manage the reference process
   4. If additional information is needed, CRM team will outreach to SAL (SAL may need to email and intro the CRM to customer)
   5. [Sales enablment video with this process](https://www.youtube.com/watch?v=8Le_Ovglnq8&list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX&index=63)

### Sample Interview Questions/topics for Customer joining the Reference Program (See [Customer Insight Page](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/) for case study process)
    * What led you to GitLab, what problems were you trying to solve?
    * Why did you choose GitLab and what other tools were you using or considering?
    * What has been your experience with GitLab?  
    * How did you make the business case for GitLab and what metrics have you seen improve?
    * What have you heard from GitLab users, what was their adoption curve like?
    * What has been the most unexpected success you have experienced? Adoption rates? Improved speed?

### Sourcing Reference Support for your sales opportunities
Please access the resources below to search for suitable references to send to your prospects/customers.
   1. [Case Reference Board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=✓&state=opened)
   2. [Metrics Reference Board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1794898)
   3. Quote Reference Board - **Coming Soon**
If you have specific reference needs that are not addressed in the resources above; please advise us on our slack channel (Customer_References) and also in the customer facing slack channels (SA/TAM/Customer Success) so we can collaborate to support you.

### Requesting a Reference Customer to support a sales call
   1. Sales team member selects "Find a reference" button from the selected opportunity Note: Opportunity required to be in stage 3 or later.
   2. Filter to find the most appropriate reference for your needs.
   3. Fill out required information and include any customer forms that may be required.
   4. Hit submit and wait for the approval for the call from the champion's account owner
   5. [Sales enablment video with this process](https://www.youtube.com/watch?v=8Le_Ovglnq8&list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX&index=63)
   6. If the sales member is unable to find the correct reference customer from within ReferenceEdge, they should post their needs in the various slack  channels (Customer success/TAM etc) to request support from customer facing teams to identify the correct customer.
   7. Please tag your regional CRM in this sourcing activity so that it can be supported and tracked.

 [Reference Edge Intro Video](https://www.point-of-reference.com/wp-content/uploads/2020/07/WhyWouldntYou-Video.mp4).

### Customer Reference Request Rules of Engagement (Managing Volume & Frequency of Reference Customer Engagement Requests)
It is critical and the Customer Reference Management team’s responsibility to optimize our reference customer engagements to balance:
* Maximizing the positive impact of both the customer’s and GitLab’s investment of time and effort
* Ensuring continuous, long term customer and GitLab success by avoiding exhausting reference customer relationships<br>

By default, we will request a customer support reference call (sales/analyst relations/public relations) no more than once per quarter per customer. In support of event speaking and/or other participation support (field, alliance, digital, community, etc.), we will request customer support at a maximum of once every two quarters per customer. The CRM ultimately decides if the reference request is suitable for the proposed customer based on a number of factors including but not limited to:
* Past, current, and future planned volume
* Frequency
* Effort required of support requests
* Customer's stated comfortable volume and levels of engagement

### Creating new issue for Customer Reference Program
To create a new issue around the Customer Reference Program, open an issue on the [Customer Reference Program Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program).
Make sure it has the label *Customer Reference Program*. Feel free to add any applicable labels around the request. Assign to Reference Program Manager as needed.

### Approved Customer Logos for promotion
GitLab understands the value of our customer relationships and values customers that are willing to share their success with our team. We have a [logo permission form](https://drive.google.com/drive/folders/1YOKNBixHcLzl9Mq-RAalrS5GpJi1MJBX) that we require be signed before we promote the customer relationship using the customer logo. We understand and respect that corporate logos are considered intellectual property and are owned and licensed by the respective organization.

- Approved List of [Customer logos](https://docs.google.com/spreadsheets/d/1wnczZQ7a8rIXI_OCNPuqJv5VeC5gGs-lwAmlgyxUlvU/edit#gid=0). This list is updated from SFDC. If you have any questions on customer logo usage, feel free to reach out to the Customer Reference Team slack Channel: Customer_References.

### Requesting a Reference Customer to speak and/or otherwise support an event
To request a reference customer to speak and/or otherwise support an event the first step is to [open the Customer Speaker Request template](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/new?issuable_template=customer-speaker-request) a minimum of 60 days before the event. This issue will be tracked on the Customer Reference Program [board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?label_name[]=Customer%20Reference%20Program).

**Process for fulfilling a request to support a Field Marketing, Corporate Marketing/Company, or Community Event**
   1. Requestor [opens the Customer Speaker Request template](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/new?issuable_template=customer-speaker-request) a minimum of 60 days before the event. This issue will be tracked on the Customer Reference Program [board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?label_name[]=Customer%20Reference%20Program).
   1. CRM will load the request into Reference Edge for tracking purposes.
   1. CRM will pull a SFDC report of customers in billing city/state/area to view an applicable pool of speakers as well as a report in Reference Edge.
   1. CRM will create a table from the report in the issue listing AE/SAL and customer accounts. CRM will tag any additional AEs/SALs in the issue.
   1. CRM will work with AEs/SALs to identify customer stories (if known).
   1. CRM will co-ordinate with the Sales and the requestor to refine the speaker pool.
   1. CRM will decide if a customer is appropriate for the outreach based upon other references that involve that customer
   1. CRM and AE/SALs will reach out with speaking request to approved customers giving customers a minimum of 45 days before the event to respond.
   1. If customer accepts, introduction to the requestor is coordinated to lead the event engagement.
   1. If customer outreach is unsuccessful (max 3 customers approached), the requestor can look at internal or other alternatives, taking their event forward without further CRM involvement.
   1. Customer Speaker Request issue is closed out once a customer has accepted to speak or if no connection with the customer was made after 2 attempts to connect. From there all communication on the logistics will take place in the main event issue and any content communication will take place in the PMM support issue, listed below.
   1. When a reference customer agree to present; the CRM should open an [SM Request issue for content and delivery support](https://gitlab.com/gitlab-com/marketing/product-marketing/-/blob/master/.gitlab/issue_templates/A-SM-Support-Request.md) with the PMM team to assist in content development and/or review and when helpful, public speaking skill development.
   1. Requestor will be mentioned in this issue to stay aware of the progress of the content development and help with prioritization.
   1. Requestor will make themselves available to assigned PMM and CRM to ensure content requirements and event themes/audience is well understood.
   1. PMM will lead content generation and content review meetings (with coordination support provided by CRM as needed) with CRM and requestor invited to attend to as optional attendees.
   1. CRM will review the final content and approve that it reflects and showcases customer story.
   1. Requestor and/or AE/SALs will provide direct logistical support (incl dry run session) to the reference customer speaker for both virtual and in-person events with the CRM informed.
   1. CRM will review the event post execution for reference opportunities and repurposing of the delivered content.
   1. CRM will update the event campaign in SFDC under the "ReferenceEdge" section with the unique speakers reference id "ReferenceProfileID". Reference id's for speakers are found under the [Reference Profile](https://gitlab.my.salesforce.com/a7H/o) section of ReferenceEdge. If an ondemand offer is created for the recorded session the CRM will update the ondemand campaign id (usually has prefix PF for Pathfactory) with the Reference ID also. Check this [SFDC report](https://gitlab.my.salesforce.com/00O4M000004aKoh) that your customer events (live and on demand) are listed.

Note: If travel is required for a customer speaker, this will not be funded by the Customer Reference Program.

**Process for fulfilling a request to support a Partner/Alliance Associated event**
   1. Requestor opens an issue on the [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program) with the label "Customer Speaking Requests" a minimum of 65 days before the event. Ideally 90 days.
   2. Please send a slack notification to the regional [Customer Reference Manager](/handbook/marketing/strategic-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact)  with a link to the issue.
   3. In the issue, please identify Partner/Alliance organization and anticipated outcome.
   4. Requestor to identify tier of customer requested. (Enterprise, Commercial, SMB)
   5. Customer Reference Manager identifies pool of proposed customers with Reference Edge, SFDC, and Alliance customer stories.
   6. CRM distributes pool to requestor and works with them to identify a short list of customers to request.
   7. CRM reaches out to appointed SALs/AEs of customers to verify timing of request.
   8. CRM or SAL/AE outreach to customer a minimum of 60 days before the event.
   9. If customer outreach is successful, the Alliance Manager is introduced to lead the engagment
   10. If customer outreach is unsuccessful (max 3 customers approached), the Alliance Manager can look at internal or other alternatives, taking their event forward without further CRM involvement.
   11. When a reference customer agrees to present; the Alliance manager should open a SM Request issue for content support with the PMM team.

Note: If travel is required for a customer speaker, it is covered by different parts of the org. (Determined on a per request basis.)

### Requesting a customer for a webinar
   1. Please open an issue on the [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program) with the label "Customer Speaking Requests" a minimum of 60 days before the event.
   2. Please send a slack notification to the regional [Customer Reference Manager](/handbook/marketing/strategic-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact)  with a link to the issue.
   3. CRM will pull a SFDC report of customers to view an applicable pool of speakers as well as a report in Reference Edge.
   4. CRM will create a table from the report in the issue listing AE/SAL and customers. CRM will tag any additional AEs/SALs in the issue.
   5. CRM will work with AEs/SALs to identify customer stories (if known).
   6. CRM coordinate with the Marketing manager to refine the speaker pool.
   7. CRM will decide if a customer is appropriate for the outreach based upon other references that involve that customer.
   8. CRM and AE/SALs will reach out with speaking request to customers giving customers a minimum of 45 days before the event to respond.
   8. If customer accepts, introduction to the marketing manager is coordinated.
   9. If customer outreach is unsuccessful, the Event Manager can look at internal alternatives and takes their event forward without further CRM involvement.
   10. When the customer agrees to present; the Marketing manager should open an SM Request issue for content support with the PMM team.

### Customer Event Presentation Process
**Process to engage with the customer once agreement for presenting at an event is secured**
   1. Customer Reference Manager (CRM) sends the customer an email (cc'ing the Event Manager as well) to request their professional photo/bio/abstract details and shares an outline for creating their success story in their corporate template.  The proposed format:
      * Landscape before GitLab: Describe the challenges/business pains experienced with CI/CD and DevOps before adopting GitLab. What problems did the customer set out to solve?
      * Why GitLab: Explain customer's reasoning for selecting GitLab. Who else was considered and/or why did we stand out?
      * Results after implementing GitLab: Talk about the positive results with GitLab at a high level. This helps set the stage for more detailed, specific metrics / numbers that validate the results in the slide that comes next.
      * Key benefits and Metrics from GitLab (align with value drivers/ use cases etc)
      * Future Plans with GitLab
    The above format is a proposed flow for the customer only; we welcome customers adding additional details (screenshots/graphic/images etc) to their presentation that reflects their success story to really make it their own. The CRM will work with the PMM to support the customer with messaging and content as required.
   2. The Event Manager supported by the CRM coordinates a maximum of 3 planning calls (including a dry run) with the customer to be respectful of everyone's time. The Event Manager schedules the planning and dry run calls in the calendars of all relevant parties as soon as feasible.<br>
      * The Event Managers act as the DRI to facilitate these calls, set the agenda, and ensure the process goes smoothly.<br>  
      * The CRM is the DRI for customer content creation only, not the overall event.<br>  
      * The CRM is optional to attend the dry run and the actual event as by then the customer content requirement has been delivered.<br>  
      * The CRM also helps coordinate between the customer and GitLab team members collaborating on the content.

### Requesting a customer for analysts
**Requesting a confidential request (Customer not named in report)**
   1. Analyst Relations team adds label "Customer Speaking Requests" to the report issue as soon as issue is opened.
   2. Analyst Relations team adds a slack message with the issue in the "Customer_References" Channel.
   3. Customer Reference Team will work with Analyst Relations manager to narrow down the pool of potential customers based on used stages etc. CRM will pull a [Gainsight report](https://gitlab.my.salesforce.com/00O4M000004aHH4) on customers using GitLab for that use case and work with internal teams on confirming the correct customers for outreach
   4. CRM to outreach to SAL/AEs for customers to get approval to speak for report. CRM will liase with the AR team re customers selected etc
   5. CRM to outreach to customer for request to speak for report. CRM to update the appropriate AR issue for this report to advise AR team.

### Requesting a customer for Public Relations activities
 1. Please open an issue on the [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program) with the label "Customer Speaking Requests"
 2. Please send a slack notification to the regional [Customer Reference Manager](/handbook/marketing/strategic-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact)  with a link to the issue.
 3. CRM will review the request and suggest customers for the PR activity to the requestor
 4. CRM will partner with the AO's for the customer outreach to request their participation in the PR activity.
 4. CRM will introduce the PR Manager to the customer to liase with them on the PR activity.

### Customer Logo Permission Form
**Purpose: To reflect our customer base we use customer logos to promote organizations that are using GitLab**

Customer logos appear on our website and other marketing and promotional materials. To respect the intellectual property of companies that use our product, we request a signed permission form to use their logo. To request logo usage from a customer, please send the [Logo permission form](https://drive.google.com/open?id=1oCRUFM4cjOTHU8DhfSBr6uO_sTYURAGo) to the appropriate member of the customer organization.
   1. Once we have received back the signed logo form, send the form to the Customer Reference Team.
   2. The Customer Reference Team will attach the signed form to the account on SFDC.
   3. The Customer Reference Team will add the customer to the Reference Edge software with logo usage selected.
   4. The Customer Reference Team will then add the logo to the customer approved locations (including website and or promotional presentations.)


### GitLab Customer Advisory Boards
Information about the GitLab Customer Advisory Boards [is found on the CAB page](/handbook/marketing/strategic-marketing/customer-reference-program/CAB/)

### Peer Reviews
Learn about our [Peer Review](/handbook/marketing/strategic-marketing/customer-reference-program/peer-reviews/) Management Program

### Custom Program Swag for Customers
The Customer Reference Team has created appreciation swag for customers to thank them for their support and for engaging with us in reference activities. Availability of these items is limited and managed by the Customer Reference Program team.
For customers in the Americas, please reach out to [Jen](mailto:jparker@gitlab.com)
For EMEA-based customers, please reach out to [Fiona](mailto:fokeeffe@gitlab.com)  

### Which customer reference team member should I contact?
  - Listed below are areas of responsibility within the Customer Reference team:

    - [Fiona](/company/team/#fokeeffe), Reference Program Manager
    - [Jen](/company/team/#jlparker), Reference Program Manager
    - [Colin](/company/team/#colinwfletcher), Manager, Market Research and Customer Insights
    - [Ashish](/company/team/#kuthiala), Senior Director, Strategic Marketing
