---
layout: handbook-page-toc
title: Talent brand
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

As a global, all-remote company, GitLab has a unique story to tell. 
A key part of telling that story is sharing what it's like to work here, because we wouldn't be successful without our people and our culture. 
This page outlines our approach to talent brand at GitLab.

### Our audience

The audience we hope to reach with our talent brand initiatives is both internal and external to GitLab:

- GitLab team members
- Job candidates and future team members
- The broader GitLab community 
- People interested in remote work 

## GitLab team member value proposition

We've defined our team member value proposition, and you can find it on the [GitLab Culture page](/company/culture/#life-at-gitlab). We focus on these tenets as we tell the story of what it's like to be part of our global, all-remote team. 


## Digital channels for talent branding

### Jobs site

Our [GitLab jobs site](/jobs/) is where candidates can find information about working at GitLab, along with a link to join our talent community. 

### Social media

We incorporate content about hiring and our culture on GitLab's [social media](/handbook/marketing/corporate-marketing/social-marketing/) accounts so that there's one central place for candidates and the community to find out more about the company as a whole.

On LinkedIn, we have a specific [career page](https://www.linkedin.com/company/gitlab-com/life) where candidates can find out more about life at GitLab. 

There are a number of videos on our [YouTube channel](https://www.youtube.com/gitlab) that relate to working here:
- [Why work remotely?](https://youtu.be/GKMUs7WXm-E)
- [Everyone can contribute](https://youtu.be/kkn32x0POTE)
- [Working remotely at GitLab](https://youtu.be/NoFLJLJ7abE)
- [This is GitLab](https://youtu.be/5QeHmiMFhDE)
- [What is GitLab?](https://youtu.be/MqL6BMOySIQ)
- [GitLab's core values](https://youtu.be/_8DFFHYAtj8)


### Review sites

We want to be sure candidates who come across GitLab's profile on employer review sites have an accurate picture of what it's like to work here. 
There are some sites where GitLab has a company profile, but we do not own it or pay for additional features. On others, we have a managed presence. 

We encourage team members to leave reviews and share their stories on these sites to continue to keep an updated profile. We welcome both positive and negative feedback in these reviews.

#### Glassdoor

##### Engaged employer

As an engaged employer with Glassdoor, we're able to customize the branded content, videos, links, and images on our Glassdoor profile. Our contract with Glassdoor includes pages in these countries:
- United States
- United Kingdom
- Germany
- The Netherlands
- India
- Ireland
- Rest of World (all countries where Glassdoor does not have a separate presence)

##### Responding to Glassdoor reviews

The [Talent Brand Manager](/job-families/people-ops/talent-branding-manager/) keeps track of new company reviews on a weekly basis (interview reviews are separate and are monitored by the Candidate Experience Specialist team), and coordinates responses when needed with input from other team members. 

###### Escalating Glassdoor reviews and tracking success

Glassdoor reviews and ratings have valuable themes and trends that can help us improve our culture, hiring process, benefits, and more. 
We share ratings trends, key themes, or escalations to the appropriate teams internally so that we can track our success and take addition action if needed. This includes:
- People Group Key meeting monthly
- Direct escalations of negative reviews - require immediate action or response
- Recruiting team meeting - sharing hiring-related themes that have come up

##### OpenCompany designation 

As an open, transparent company, the OpenCompany designation is important for us to have to best represent our talent brand. We take action to maintain this status throughout the year.

Achieving and maintaining OpenCompany requires that you:
- Keep company profile up to date
- Add 5-10 new photos every 12 months
- Get 5-60 new employee reviews (depending on company size) every 12 months
- Respond to 2-10 reviews (depending on company size) every 12 months
- Promote your profile with a link on your career site

##### Profile updates on Glassdoor 

To be sure the details on our profile stay up to date, we [review these items](https://gitlab.com/gl-recruiting/talent-brand/-/issues/9) quarterly and make any needed updates:
- Headcounts and country numbers listed on our profile 
- Our status as the world's largest all-remote company
- New awards or recognition 
- Photos or videos recently published


#### Comparably

We have a [premium profile with Comparably](https://www.comparably.com/companies/gitlab), an employer review site that also offers recruitment marketing tools and [award programs](/handbook/people-group/employment-branding/#employer-awards-and-recognition). 

Our goal is to keep the feedback on our profile up to date by annually (Q3) sending a link to all team members where they can consider leaving feedback or a review. 

#### Other employer sites

- [RemoteHub](https://remotehub.io/gitlab)
- [Indeed](https://www.indeed.com/cmp/Gitlab-Inc/about)
- [AngelList](https://angel.co/company/gitlab/)
- [RemoteFit](https://www.remotefit.io/c/gitlab)

### GitLab blog

To give the most authentic view of life at GitLab, we encourage team members to blog about their experiences. 
You can find many of these posts in the [culture section](/blog/categories/culture/) of the GitLab blog. 

### HackerNews

We promote life at GitLab and our talent community on [HackerNews](https://news.ycombinator.com/). 

#### Who's Hiring monthly post

On the first of the month (or closest business day after) at 11 a.m. ET, the Recruiting team will post a comment for GitLab in the Hacker News thread called ["Who's Hiring"](https://news.ycombinator.com/ask). 
Here's a template that will be updated monthly by the Talent Brand Manager:

*Template text (updated for 2020-07-01):* 

GitLab, Remote only, Full time

As the world’s largest all-remote company, GitLab is a place where you can contribute from almost anywhere. We're an ambitious, productive team that embraces a set of shared values in everything we do. 

As our team continues to grow, we're taking a unique, outbound approach to hiring. If you're interested in current or future roles at GitLab, share your information with our recruiters by joining our Talent Community: https://grnh.se/8490b7772us  

Check out this video from our Recruiting team to learn more about the Talent Community and how we hire: https://youtu.be/uK_kO8FcgpA 


**HackerNews Notes:**
- Sid's HackerNews credentials should not to be used for the "Who's Hiring" post. The team member posting the comment should use their own account. 
- When posting on HackerNews, remember that it is our most important social channel. Please follow the [best practices](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews/#best-practices) and [social media guidelines](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews/#social-media-guidelines). 

## Talent brand resources

### Hiring fact sheet

The hiring fact sheet is a quick reference guide that offers a high-level overview of working at GitLab, including our latest headcount, employer awards, and blogs.
It links to a number of places in the GitLab handbook as the single source of truth. 

The fact sheet is updated monthly, so be sure to [download the latest one](https://gitlab.com/gitlab-com/people-group/recruiting/-/blob/master/Hiring_fact_sheet_-_June_2020.pdf).

### Content library

The content library is a curated list of blog posts, articles, videos, awards, and quick facts that help tell the story of life at GitLab. (WIP coming soon). 

### GitLab talent ambassadors

Spread the word about life at GitLab by becoming a GitLab talent ambassdor. 

Whether you're a hiring manager or an individual contributor, [this page](/handbook/hiring/gitlab-ambassadors) outlines the steps you can take to help represent our talent brand and attract more great people to join the team.

## Employer awards and recognition

- [Fortune's Best Small & Medium Workplaces in 2020](https://www.greatplacetowork.com/best-workplaces/smb/2020?category=medium)
- [Great Place to Work Certified](https://www.greatplacetowork.com/certified-company/7013799)
- [Best Startup Employers in 2020, Forbes](https://www.forbes.com/americas-best-startup-employers/#46146ae96527)
- [2020 Cloud 100 List, Forbes](https://www.forbes.com/cloud100/#12ceb6e35f94)
- [No. 2 top private employer via Hired's Brand Health Report](https://www-forbes-com.cdn.ampproject.org/c/s/www.forbes.com/sites/johnkoetsier/2020/09/22/the-top-40-brands-people-want-to-work-for-in-the-tech-industry/amp/)
- [Happiest Employees, Comparably](https://www.businessinsider.com/top-companies-employees-happy-fulfilled-comparably-2020-10)
- [Best Perks and Benefits, Comparably](https://www.businessinsider.com/comparably-big-companies-best-perks-employee-benefits-2020-10)
- [Best Compensation, Comparably](https://www.businessinsider.com/best-paying-big-companies-comparably-salary-2020-10)
- [Best Work Life Balance, Comparably](https://www.businessinsider.com/best-companies-if-you-want-to-achieve-work-life-balance-2020-10)
- [Top 100 Remote Work Companies 2020, FlexJobs](https://www.flexjobs.com/blog/post/100-top-companies-with-remote-jobs-2020/)
- [Best Workplaces in 2019, Inc.](/blog/2019/05/16/building-an-award-winning-culture-at-gitlab/)
- [18 Great Companies For Millennials in the San Francisco Area](https://www.comparably.com/articles/18-great-companies-for-millennials-in-the-san-francisco-area/)
- [Best Company Culture, Comparably](https://www.comparably.com/news/best-company-culture-2019/)
- [Best Companies for Women, Comparably](https://www.comparably.com/news/best-companies-for-women-2019/)
- [Best Companies for Diversity, Comparably](https://www.comparably.com/news/best-companies-for-diversity-2019/)

Here's an [epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/565) with an overview of some of our employer award programs. 

## All-remote work

A foundational aspect of our talent brand is the flexibility that all-remote work gives our team members. Learn more about GitLab's approach to remote work on our [all-remote page](/company/culture/all-remote/). 

## Performance indicators

Here are the definitions for the performance indicators listed in the talent brand job family. 

### Glassdoor engagement

- Company rating
- Page views
- Followers
- Apply starts on jobs

### LinkedIn Talent Brand metrics

- Career page impressions
- Percentage of page visitors who view jobs
- Percentage of new hires who visit our page

### Team member engagement score

Our team member engagement or net promoter ("I would recommend GitLab as a great place to work.") score is measured annually in the Culture Amp survey. 
Here are samples of the statements team members were asked to consider:
- I would recommend GitLab as a great place to work
- GitLab motivates me to go beyond what I would in a similar role elsewhere
- I am proud to work for GitLab
- I rarely think about looking for a job at another company
- I see myself still working at GitLab in two years' time

### Team member turnover

[Defined](/handbook/people-group/people-operations-metrics/#team-member-turnover) on the People Group Metrics page.

### Team member referrals 

Overall number of job candidate referrals from GitLab team members. 

### Hires vs. plan

[Defined](/handbook/hiring/metrics/#hires-vs-plan) on the People Group Metrics page.

